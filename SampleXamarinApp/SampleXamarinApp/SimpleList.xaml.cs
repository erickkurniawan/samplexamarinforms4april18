﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SampleXamarinApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SimpleList : ContentPage
    {
        public SimpleList()
        {
            InitializeComponent();
            List<string> items = new List<string>() { "Erick","Budi","Amir","Deny" };
            myListView.ItemsSource = items;

            myListView.ItemTapped += MyListView_ItemTapped;

            btnImageList.Clicked += BtnImageList_Clicked;
           
        }

        private async void MenuAdd_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new ProductListForm());
        }

        private async void BtnImageList_Clicked(object sender, EventArgs e)
        {
            //var username = txtUsername.Text;
            await Navigation.PushAsync(new ImageListView(txtUsername.Text));
        }

        private void MyListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            DisplayAlert("Tapped", "Anda memilih : " + e.Item.ToString(), "OK");
            ((ListView)sender).SelectedItem = null;
        }
    }
}