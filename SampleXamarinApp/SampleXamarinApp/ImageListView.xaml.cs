﻿using SampleXamarinApp.Models;
using SampleXamarinApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SampleXamarinApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ImageListView : ContentPage
    {
        private string _username;
        public ImageListView()
        {
            InitializeComponent();
            BindingContext = new ProductViewModel();
            menuProductList.Clicked += MenuProductList_Clicked;
        }
        public ImageListView(string username)
        {
            InitializeComponent();
            BindingContext = new ProductViewModel();
            _username = username;
            menuProductList.Clicked += MenuProductList_Clicked;
            menuUser.Clicked += MenuUser_Clicked;
        }

        private async void MenuUser_Clicked(object sender, EventArgs e)
        {
            await DisplayAlert("Username", "Username: " + _username, "OK");
        }

        private async void MenuProductList_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ProductListForm());
        }

        private void ListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var product = (Product)e.Item;
            DisplayAlert("Keterangan", "Product " + product.Nama,"OK");
        }
    }
}