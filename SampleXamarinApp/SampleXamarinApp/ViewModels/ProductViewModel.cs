﻿using SampleXamarinApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace SampleXamarinApp.ViewModels
{
    public class ProductViewModel : BindableObject
    {
        private List<Product> _listProduct;
        public List<Product> ListProduct
        {
            get { return _listProduct; }
            set { _listProduct = value; OnPropertyChanged("ListProduct"); }
        }

        public ProductViewModel()
        {
            ListProduct = new List<Product>
            {
                new Product{Kode="AA111",Nama="Tas",Quantity=2,Pic="sample1.png"},
                new Product{Kode="AA222",Nama="T-Shirt",Quantity=3,Pic="sample2.png"},
                new Product{Kode="AA333",Nama="Shoes",Quantity=4,Pic="sample3.png"}
            };
        }
    }
}
