﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SampleXamarinApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SampleTabbedPage : TabbedPage
    {
        public SampleTabbedPage ()
        {
            InitializeComponent();
            btnAction.Clicked += BtnAction_Clicked;
        }

        private async void BtnAction_Clicked(object sender, EventArgs e)
        {
            var action = await DisplayActionSheet("Action Sheet","Cancel","Delete","Photo Roll","Email");

        }
    }
}