﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SampleXamarinApp
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            //btnHitung.Clicked += BtnHitung_Clicked;

        }

        private void BtnHitung_Clicked(object sender, EventArgs e)
        {
            var btn = (Button)sender;
            double bil1 = Convert.ToDouble(txtBil1.Text);
            double bil2 = Convert.ToDouble(txtBil2.Text);
            double hasil = 0;

            /*switch (btn.Text)
            {
                case "Kali":
                    hasil = bil1 * bil2;
                    break;
                default:
                    hasil = bil1 + bil2;
                    break;
            }*/

            if (btn.Text == "Kali")
            {
                hasil = bil1 * bil2;
            }
            else
            {
                hasil = bil1 + bil2;
            }
            DisplayAlert("Informasi", "Hasil: " + hasil, "OK");
        }
    }
}
