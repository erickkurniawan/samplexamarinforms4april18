﻿using SampleXamarinApp.Models;
using SampleXamarinApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SampleXamarinApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ProductListForm : ContentPage
	{
		public ProductListForm ()
		{
			InitializeComponent ();
            BindingContext = new ProductViewModel();
            btnClose.Clicked += BtnClose_Clicked;
		}

        private async void BtnClose_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }

        private async void MyList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
           
            var product = (Product)e.Item;
            await DisplayAlert("Pilihan", "Data " + product.Kode + " - " + 
                product.Nama+" - "+product.Quantity.ToString(),"OK");
        }
    }
}