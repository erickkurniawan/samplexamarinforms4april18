﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SampleXamarinApp.Models
{
    public class Product
    {
        public string Kode { get; set; }
        public string Nama { get; set; }
        public int Quantity { get; set; }
        public string Pic { get; set; }
    }
}
